﻿using StudentManagement.RemoteStudentService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudentManagement
{
    public partial class Form1 : Form
    {
        private StudentService remoteServer = new StudentService();

        public Form1()
        {
            InitializeComponent();
            
        }

        private void loadStudents()
        {
            List<Student> studentList = new List<Student>(remoteServer.load());
            if (studentList == null)
            {
                MessageBox.Show("No data to load");
                return;
            }
            this.richTextBox_list.Text = "";
            for (int i = 0; i < studentList.Count; i++)
            {
                this.richTextBox_list.Text += (studentList[i].Id + "\n"
                                            + studentList[i].Name + "\n"
                                            + studentList[i].Grade.ToString() + "\n"
                                            + "-----------------------\n");


            }
        }

        private void button_load_Click(object sender, EventArgs e)
        {
            this.loadStudents();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            Student newStudent = new Student();
            newStudent.Id = this.textBox_addId.Text;
            newStudent.Name = this.textBox_addName.Text;
            newStudent.Grade = float.Parse(this.textBox_addGrade.Text);
            if (remoteServer.addStudent(newStudent))
            {
                MessageBox.Show("Add successfully");
                this.loadStudents();
            }
            else
            {
                MessageBox.Show("Add failed");
                this.loadStudents();
            }
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            string targetStudentId = textBox_deleteId.Text;
            if (remoteServer.deleteStudent(targetStudentId))
            {
                MessageBox.Show("Delete successfully");
                this.loadStudents();
            }
            else
            {
                MessageBox.Show("Delete failed");
                this.loadStudents();
            }
        }



    }
}
