﻿namespace StudentManagement
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox_list = new System.Windows.Forms.RichTextBox();
            this.button_load = new System.Windows.Forms.Button();
            this.textBox_addId = new System.Windows.Forms.TextBox();
            this.textBox_addName = new System.Windows.Forms.TextBox();
            this.textBox_addGrade = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_add = new System.Windows.Forms.Button();
            this.textBox_deleteId = new System.Windows.Forms.TextBox();
            this.button_delete = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBox_list
            // 
            this.richTextBox_list.Location = new System.Drawing.Point(118, 12);
            this.richTextBox_list.Name = "richTextBox_list";
            this.richTextBox_list.Size = new System.Drawing.Size(236, 393);
            this.richTextBox_list.TabIndex = 0;
            this.richTextBox_list.Text = "";
            // 
            // button_load
            // 
            this.button_load.Location = new System.Drawing.Point(12, 22);
            this.button_load.Name = "button_load";
            this.button_load.Size = new System.Drawing.Size(75, 23);
            this.button_load.TabIndex = 1;
            this.button_load.Text = "Load";
            this.button_load.UseVisualStyleBackColor = true;
            this.button_load.Click += new System.EventHandler(this.button_load_Click);
            // 
            // textBox_addId
            // 
            this.textBox_addId.Location = new System.Drawing.Point(12, 74);
            this.textBox_addId.Name = "textBox_addId";
            this.textBox_addId.Size = new System.Drawing.Size(100, 20);
            this.textBox_addId.TabIndex = 2;
            // 
            // textBox_addName
            // 
            this.textBox_addName.Location = new System.Drawing.Point(12, 124);
            this.textBox_addName.Name = "textBox_addName";
            this.textBox_addName.Size = new System.Drawing.Size(100, 20);
            this.textBox_addName.TabIndex = 3;
            // 
            // textBox_addGrade
            // 
            this.textBox_addGrade.Location = new System.Drawing.Point(12, 171);
            this.textBox_addGrade.Name = "textBox_addGrade";
            this.textBox_addGrade.Size = new System.Drawing.Size(100, 20);
            this.textBox_addGrade.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "Grade";
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(12, 197);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(75, 23);
            this.button_add.TabIndex = 8;
            this.button_add.Text = "Add";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // textBox_deleteId
            // 
            this.textBox_deleteId.Location = new System.Drawing.Point(12, 263);
            this.textBox_deleteId.Name = "textBox_deleteId";
            this.textBox_deleteId.Size = new System.Drawing.Size(100, 20);
            this.textBox_deleteId.TabIndex = 9;
            // 
            // button_delete
            // 
            this.button_delete.Location = new System.Drawing.Point(12, 289);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(75, 23);
            this.button_delete.TabIndex = 10;
            this.button_delete.Text = "Delete";
            this.button_delete.UseVisualStyleBackColor = true;
            this.button_delete.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 243);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 14);
            this.label4.TabIndex = 11;
            this.label4.Text = "ID";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 417);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button_delete);
            this.Controls.Add(this.textBox_deleteId);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_addGrade);
            this.Controls.Add(this.textBox_addName);
            this.Controls.Add(this.textBox_addId);
            this.Controls.Add(this.button_load);
            this.Controls.Add(this.richTextBox_list);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Student Management";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox_list;
        private System.Windows.Forms.Button button_load;
        private System.Windows.Forms.TextBox textBox_addId;
        private System.Windows.Forms.TextBox textBox_addName;
        private System.Windows.Forms.TextBox textBox_addGrade;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.TextBox textBox_deleteId;
        private System.Windows.Forms.Button button_delete;
        private System.Windows.Forms.Label label4;

    }
}

