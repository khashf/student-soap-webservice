﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentService
{
    public class Student
    {
        private string id;
        private string name;
        private float grade;

        public string Id { get; set; }
        public string Name { get; set; }
        public float Grade { get; set; }
    }
}