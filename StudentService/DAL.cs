﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace StudentService
{
    public class DAL
    {
        SqlConnection con;
        public DAL()
        {
            con = new SqlConnection(@"Data Source=CAMKHUONG;Initial Catalog=STUDENTDB;Integrated Security=True");
        }
        
        public List<Student> load()
        {
            List<Student> studentList = new List<Student>();
            SqlCommand cmd = new SqlCommand(@"Select * from dbo.student", con);
            con.Open();
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Student tempStudent = new Student();
                    tempStudent.Id = reader[0].ToString();
                    tempStudent.Name = reader[1].ToString();
                    tempStudent.Grade = (float)reader[2];
                    studentList.Add(tempStudent);
                }
                //reader.Close();
                con.Close();
            }
            catch (Exception e)
            {
                con.Close();
                return null;
            }
            return studentList;
        }

        public bool add(Student newStudent)
        {
            string query = "Insert into dbo.student values("
                            + "'" + newStudent.Id + "'" + ","
                            + "'" + newStudent.Name + "'" + ","
                            + newStudent.Grade + ")";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            try
            {
                int effectRows = cmd.ExecuteNonQuery();
                con.Close();
                if (effectRows <= 0)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                con.Close();
                return false;
            }
            
        }

        public bool delete(string targetStudentId)
        {
            string query = "Delete from dbo.student where id='" + targetStudentId + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            try
            {
                int effectRows = cmd.ExecuteNonQuery();
                con.Close();
                if (effectRows <= 0)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                con.Close();
                return false;
            }

        }

    }
}