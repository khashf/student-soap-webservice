﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace StudentService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class StudentService : System.Web.Services.WebService
    {
        DAL dataAccess = new DAL();
        [WebMethod]
        public List<Student> load()
        {
            return dataAccess.load();
        }

        [WebMethod]
        public bool addStudent(Student newStudent)
        {
            return dataAccess.add(newStudent);
        }

        [WebMethod]
        public bool deleteStudent(string targetStudentId)
        {
            return dataAccess.delete(targetStudentId);
        }
    }
}